name := "spark-explo"

version := "1.0"

scalaVersion := "2.11.12"

val sparkVersion = "2.4.3"

libraryDependencies += "org.apache.spark" %% "spark-sql" % sparkVersion
libraryDependencies += "org.apache.spark" %% "spark-sql-kafka-0-10" % sparkVersion
