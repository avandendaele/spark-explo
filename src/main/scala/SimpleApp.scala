import java.util.Properties

import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.common.TopicPartition
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.sql.SparkSession

object SimpleApp {

  val topic = "spark-explo"

  val kafkaConsumerProps = new Properties()
  kafkaConsumerProps.setProperty("", "localhost:9092")
  kafkaConsumerProps.setProperty("", "local-spark-explo")

  val kafkaConsumer = new KafkaConsumer[String, String](kafkaConsumerProps, new StringDeserializer, new StringDeserializer)
  val partitionCount: Int = kafkaConsumer.partitionsFor(topic).size()
  Seq.tabulate(partitionCount) { p =>
    val tp = new TopicPartition(topic, p)
    tp -> kafkaConsumer.committed(tp)
  }


  val ss = SparkSession.builder().getOrCreate()
  ss.readStream
    .csv("")



}